FROM ubuntu:18.10
# disable interactive mode
ENV DEBIAN_FRONTEND=noninteractive
# prepare os to work
RUN apt update
RUN apt upgrade -y
RUN apt install -y \
                apache2-bin \
                libapache2-mod-php7.2 \
                php7.2 \
                php7.2-bcmath \
                php7.2-bz2 \
                php7.2-gd \
                php7.2-imap \
                php7.2-intl \
                php7.2-json \
                php7.2-mbstring \
                php7.2-mysql \
                php7.2-xml \
                php7.2-zip \
                supervisor \
                tzdata
RUN cp /usr/share/zoneinfo/Europe/Warsaw /etc/localtime
# copy root
ADD rootfs /
# inform docker about using http port
EXPOSE 80
# start apache2
CMD apachectl -D FOREGROUND
